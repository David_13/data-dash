Mode d'Emploi pour Accéder à notre Site via EduPython

Bienvenue ! Pour accéder à notre site, veuillez suivre attentivement ces instructions. Assurez-vous d'avoir installé EduPython sur votre système avant de commencer.

Étape 1 : Installation d'EduPython

Téléchargez EduPython à partir de site de téléchargement officiel d'EduPython.
Suivez les instructions d'installation fournies par le programme d'installation. Assurez-vous de noter le chemin d'installation de EduPython sur votre système.

Étape 2 : Lancement du Serveur

Ouvrez l'Explorateur de fichiers de votre système.
Naviguez vers le dossier où vous avez votre fichier "servers_start.py".
Double-cliquez sur le fichier "servers_start.py" pour exécuter le programme.
Étape 3 : Copie du lien HTTP

Une fois le programme exécuté, il devrait démarrer un serveur local.
Sur la console ou l'interface d'EduPython, recherchez le lien HTTP affiché. Il devrait ressembler à quelque chose comme http://127.0.0.1:5000.

Étape 4 : Accès au Site

Ouvrez votre navigateur web préféré.
Dans la barre d'adresse du navigateur, collez le lien HTTP que vous avez copié depuis la console d'EduPython.
Appuyez sur Entrée pour accéder à notre site.
