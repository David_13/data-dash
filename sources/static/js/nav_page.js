// JavaScript pour défilement vers les sections correspondantes
$(document).ready(function() {
    $(".nav-button").click(function() {
        var target = "#" + $(this).data("target");
        $('html, body').animate({
            scrollTop: $(target).offset().top
        }, 1000);
    });
});
