document.addEventListener("DOMContentLoaded", function () {
    const themeToggleBtn = document.getElementById("themeToggle");

    themeToggleBtn.addEventListener("click", function () {
        // Utilisez la classe 'theme_2' pour basculer le thème
        document.documentElement.classList.toggle("theme_2");

        // Stockez le thème actuel dans un cookie avec une expiration d'une journée
        const newTheme = document.documentElement.classList.contains("theme_2") ? "theme_2" : "theme_base";
        document.cookie = `selectedTheme=${newTheme}; expires=${new Date(Date.now() + 86400000).toUTCString()}`;
    });

    // Récupérez le thème sélectionné depuis le cookie et appliquez-le au chargement de la page
    const selectedTheme = document.cookie.split(';').find(cookie => cookie.trim().startsWith('selectedTheme='));
    if (selectedTheme) {
        const themeValue = selectedTheme.split('=')[1];
        document.documentElement.classList.add(themeValue);
    }
});
