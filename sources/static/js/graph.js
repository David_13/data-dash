document.addEventListener('DOMContentLoaded', function () {
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: Array.from({ length: dataSets[0].data.length }, (_, i) => `Point ${i + 1}`),
            datasets: dataSets,
        },
        options: {
            tooltips: {
                mode: 'index',
                intersect: false,
            },
        },
    });
});
