document.getElementById('resetButton').addEventListener('click', function() {
    var confirmation = confirm("Êtes-vous sûr de vouloir réinitialiser toutes les tables ?");
    if (confirmation) {
        // Envoi de la requête AJAX pour réinitialiser les tables
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/supp_table', true);
        xhr.onload = function () {
            alert('Toutes les tables ont été réinitialisées avec succès !');
            setTimeout(function() {
                window.location.reload();
            }, 1);
        };
        xhr.send();
    }
});
