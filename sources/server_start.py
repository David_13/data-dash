# Programme principal

import os
from flask import Flask, render_template, request, jsonify
import random
import sqlite3
from modules.temps_exe import temps_execution
from modules.algo_tri import tri_insertion, tri_selection, tri_sorted, tri_fusion, fusionner, tri_bulles
from modules.base_donnees import sauvegarder_donnees, sauvegarde_elements, liste_existe, recuperer_elements
from modules.reset_database import reset_database

#_______________________________________________________________________________

app = Flask(__name__, template_folder='templates')

@app.route('/')
def index():
    """
    Route pour la page principale.
    """
    return render_template("index.html")

# Définissez des variables globales pour stocker les valeurs
insertion_utilisateur = None
selection_utilisateur = None
sorted_utilisateur = None
fusion_utilisateur = None
bulles_utilisateur = None
nom_utilisateur = None
elements = []
liste_a_trier = []
nbe_elements = None
app.template_folder = 'templates'

@app.route('/page2', methods=['GET', 'POST'])
def page2():
    """
    Route pour la page 2, gère l'entrée de l'utilisateur et effectue des algorithmes de tri.
    """
    global insertion_utilisateur, selection_utilisateur, sorted_utilisateur, \
    fusion_utilisateur, bulles_utilisateur, nom_utilisateur,\
    nombres_elements_utilisateur

    donees_insertion = []
    donees_sorted = []
    donees_selection = []
    donees_fusion = []
    donees_bulles = []

    if request.method == 'POST':
        insertion_utilisateur = request.form.get('tri_insertion')
        selection_utilisateur = request.form.get('tri_selection')
        sorted_utilisateur = request.form.get('tri_sorted')
        fusion_utilisateur = request.form.get('tri_fusion')
        bulles_utilisateur = request.form.get('tri_bulles')
        nom_utilisateur = request.form.get('triName')
        nombres_elements_utilisateur = int(request.form.get('nombreChiffres'))

        # Affiche l'entrée de l'utilisateur à des fins de débogage
        print(f'Tri Insertion: {insertion_utilisateur}')
        print(f'Tri Selection: {selection_utilisateur}')
        print(f'Tri Sorted: {sorted_utilisateur}')
        print(f'Tri Fusion: {fusion_utilisateur}')
        print(f'Tri Bulles: {bulles_utilisateur}')
        print(f'Tri Name: {nom_utilisateur}')
        print(f'Nombre Chiffres: {nombres_elements_utilisateur}')

        # Initialise des dictionnaires pour stocker les résultats
        results_insertion = {'temps_total': 0, 'nombre_elements_total': 0}
        results_selection = {'temps_total': 0, 'nombre_elements_total': 0}
        results_sorted = {'temps_total': 0, 'nombre_elements_total': 0}
        results_fusion = {'temps_total': 0, 'nombre_elements_total': 0}
        results_bulles = {'temps_total': 0, 'nombre_elements_total': 0}

        nbe_elements = liste_existe(nombres_elements_utilisateur)

        for n in range(nombres_elements_utilisateur):
            if liste_existe(nombres_elements_utilisateur):
                elements = recuperer_elements(nombres_elements_utilisateur)
                liste_a_trier = recuperer_elements(nombres_elements_utilisateur)
            else:
                # Génère la liste à trier
                liste_a_trier = [random.randint(1, 1000) for _ in range(n)]
                elements = liste_a_trier

            # Copie de la liste pour tri_sorted car il modifie la liste en place
            liste_a_trier_sorted = liste_a_trier[:]

            # Tri par insertion
            temps_insertion = temps_execution(tri_insertion, liste_a_trier[:])
            results_insertion['temps_total'] += temps_insertion
            donees_insertion.append(temps_insertion)
            results_insertion['nombre_elements_total'] += n

            # Tri par sélection
            temps_selection = temps_execution(tri_selection, liste_a_trier[:])
            results_selection['temps_total'] += temps_selection
            donees_selection.append(temps_selection)
            results_selection['nombre_elements_total'] += n

            # Tri avec sorted
            temps_sorted = temps_execution(tri_sorted, liste_a_trier_sorted)
            results_sorted['temps_total'] += temps_sorted
            donees_sorted.append(temps_sorted)
            results_sorted['nombre_elements_total'] += n

            # Tri par fusion
            temps_fusion = temps_execution(tri_fusion, liste_a_trier[:])
            results_fusion['temps_total'] += temps_fusion
            donees_fusion.append(temps_fusion)
            results_fusion['nombre_elements_total'] += n

            # Tri par bulles
            temps_bulles = temps_execution(tri_bulles, liste_a_trier[:])
            results_bulles['temps_total'] += temps_bulles
            donees_bulles.append(temps_bulles)
            results_bulles['nombre_elements_total'] += n

        # Sauvegarde les résultats dans la base de données
        sauvegarder_donnees(nom_utilisateur, "Tri par Insertion", \
        n, results_insertion['temps_total'], donees_insertion)
        sauvegarder_donnees(nom_utilisateur, "Tri par Sélection", \
        n, results_selection['temps_total'], donees_selection)
        sauvegarder_donnees(nom_utilisateur, "Tri avec Sorted", \
        n, results_sorted['temps_total'], donees_sorted),\
        sauvegarder_donnees(nom_utilisateur, "Tri par fusion", \
        n, results_fusion['temps_total'], donees_fusion),\
        sauvegarder_donnees(nom_utilisateur, "Tri par bulles", \
        n, results_bulles['temps_total'], donees_bulles)

        if liste_existe(nombres_elements_utilisateur):
            pass
        else:
            sauvegarde_elements(n,elements)

    # Récupère les données sauvegardées depuis la base de données
    connection = sqlite3.connect('donnees_mesurees.db')
    cursor = connection.cursor()
    cursor.execute("SELECT DISTINCT nom_utilisateur, algorithme, \
    SUM(temps_mesure) as temps_total, taille, MAX(date_heure) \
    as date_heure FROM mesures GROUP BY nom_utilisateur, \
    algorithme ORDER BY nom_utilisateur, algorithme, date_heure DESC")
    data = cursor.fetchall()
    cursor.close()
    connection.close()

    data_sets = [
        {
            'label': 'tri insertion',
            'data': donees_insertion,
            'borderColor': 'rgba(75, 192, 192, 1)',
            'fill': False,
        },
        {
            'label': 'tri selection',
            'data': donees_selection,
            'borderColor': 'rgba(255, 99, 132, 1)',
            'fill': False,
        },
        {
            'label': 'tri sorted',
            'data': donees_sorted,
            'borderColor': 'rgba(255, 206, 86, 1)',
            'fill': False,
        },
        {
            'label': 'tri fusion',
            'data': donees_fusion,
            'borderColor': 'rgba(0, 0, 0, 1)',
            'fill': False,
        },
        {
            'label': 'tri bulles',
            'data': donees_bulles,
            'borderColor': 'rgba(164, 45, 165, 1)',
            'fill': False,
        },
    ]

    return render_template("tests.html", data_sets=data_sets)

@app.route('/get_chart_data')
def get_chart_data():
    """
    Route pour récupérer les données du graphique depuis la base de données.
    """
    connection = sqlite3.connect('donnees_mesurees.db')
    cursor = connection.cursor()
    cursor.execute("SELECT algorithme, donees FROM mesures")
    data = cursor.fetchall()
    cursor.close()
    connection.close()

    return jsonify(data)

@app.route('/page3')
def page3():
    """
    Route pour afficher les données de chaque table dans le modèle HTML.
    """
    # Récupérer les données de chaque table
    connection = sqlite3.connect('donnees_mesurees.db')
    cursor = connection.cursor()

    # Définir le mapping des noms de tables à afficher dans le modèle HTML
    table_names_mapping = {
        'mesures_bulle': 'Tri Bulle',
        'mesures_fusion': 'Tri Fusion',
        'mesures_insertion': 'Tri Insertion',
        'mesures_selection': 'Tri Selection',
        'mesures_sorted': 'Tri Sorted'
    }

    # Récupérer les données de chaque table et stocker dans un dictionnaire
    tables_data = {}
    tables = ['mesures_bulle', 'mesures_fusion', 'mesures_insertion', 'mesures_selection', 'mesures_sorted']
    for table in tables:
        cursor.execute(f"SELECT * FROM {table}")
        tables_data[table_names_mapping[table]] = cursor.fetchall()

    cursor.close()
    connection.close()

    return render_template("tables.html", tables_data=tables_data)

@app.route('/supp_table')
def supp_table():
    """
    Route pour réinitialiser la base de données.
    """
    reset_database()
    return "Les tables ont été réinitialisées avec succès."

@app.route('/page4')
def page4():
    """
    Route pour afficher la page "À propos".
    """
    return render_template("a_propos.html")

@app.route('/page5')
def page5():
    """
    Route pour afficher la page "Contacts".
    """
    return render_template("contacs.html")

if __name__ == '__main__':
    app.run(debug=True)
