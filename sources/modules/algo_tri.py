import threading

def tri_selection(liste):
    """
    Implémente l'algorithme de tri par sélection.
    """
    n = len(liste)
    for i in range(n):
        min_idx = i
        for j in range(i + 1, n):
            if liste[j] < liste[min_idx]:
                min_idx = j
        liste[i], liste[min_idx] = liste[min_idx], liste[i]
    return liste

def tri_insertion(liste):
    """
    Implémente l'algorithme de tri par insertion.
    """
    for i in range(1, len(liste)):
        cle = liste[i]
        j = i - 1
        while j >= 0 and cle < liste[j]:
            liste[j + 1] = liste[j]
            j -= 1
        liste[j + 1] = cle

def tri_sorted(liste):
    """
    Utilise la fonction sorted() pour trier la liste.
    """
    return sorted(liste)

def tri_bulles(arr):
    """
    Implémente l'algorithme de tri à bulles.
    """
    n = len(arr)
    for i in range(n):
        for j in range(0, n-i-1):
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]

def fusionner(gauche, droite):
    """
    Fusionne deux listes triées.
    """
    fusion = []
    while gauche and droite:
        if gauche[0] <= droite[0]:
            fusion.append(gauche.pop(0))
        else:
            fusion.append(droite.pop(0))
    fusion.extend(gauche)
    fusion.extend(droite)
    return fusion

def tri_fusion(tableau):
    """
    Implémente l'algorithme de tri fusion.
    """
    if len(tableau) <= 1:
        return tableau
    milieu = len(tableau) // 2
    gauche = tableau[:milieu]
    droite = tableau[milieu:]
    gauche = tri_fusion(gauche)
    droite = tri_fusion(droite)
    return fusionner(gauche, droite)
