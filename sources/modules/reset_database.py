import sqlite3

def reset_database():
    """
    Réinitialise la base de données en supprimant toutes les tables et en les recréant.
    """
    try:
        # Connectez-vous à la base de données
        connection = sqlite3.connect('donnees_mesurees.db')
        cursor = connection.cursor()

        # Liste des tables à supprimer
        tables_to_drop = [
            "mesures_insertion",
            "mesures_selection",
            "mesures_sorted",
            "mesures_bulle",
            "mesures_fusion"
            # Ajoutez ici d'autres tables si nécessaire
        ]

        # Supprimez chaque table si elle existe
        for table_name in tables_to_drop:
            cursor.execute(f"DROP TABLE IF EXISTS {table_name}")

        # Recréez les tables
        cursor.execute("CREATE TABLE IF NOT EXISTS mesures_insertion (id INTEGER PRIMARY KEY, nom_utilisateur TEXT, algorithme TEXT, taille INTEGER, temps_mesure REAL, date_heure TEXT)")
        cursor.execute("CREATE TABLE IF NOT EXISTS mesures_selection (id INTEGER PRIMARY KEY, nom_utilisateur TEXT, algorithme TEXT, taille INTEGER, temps_mesure REAL, date_heure TEXT)")
        cursor.execute("CREATE TABLE IF NOT EXISTS mesures_sorted (id INTEGER PRIMARY KEY, nom_utilisateur TEXT, algorithme TEXT, taille INTEGER, temps_mesure REAL, date_heure TEXT)")
        cursor.execute("CREATE TABLE IF NOT EXISTS mesures_bulle (id INTEGER PRIMARY KEY, nom_utilisateur TEXT, algorithme TEXT, taille INTEGER, temps_mesure REAL, date_heure TEXT)")
        cursor.execute("CREATE TABLE IF NOT EXISTS mesures_fusion (id INTEGER PRIMARY KEY, nom_utilisateur TEXT, algorithme TEXT, taille INTEGER, temps_mesure REAL, date_heure TEXT)")

        # Validez les modifications
        connection.commit()

        print("Toutes les tables de la base de données ont été réinitialisées avec succès.")

    except Exception as e:
        print(f"Une erreur s'est produite : {e}")

    finally:
        # Fermez la connexion à la base de données
        if connection:
            connection.close()

if __name__ == "__main__":
    # Appelez la fonction pour réinitialiser la base de données
    reset_database()