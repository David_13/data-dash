import time
import threading

def temps_execution(tri_fonction, tableau):
    """
    Mesure le temps d'exécution d'une fonction de tri.
    """
    debut = time.time()
    tri_fonction(tableau)
    fin = time.time()
    temps_mesure = fin - debut
    return temps_mesure