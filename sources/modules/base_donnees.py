import time
import sqlite3
import threading
import json

def sauvegarder_donnees(nom_utilisateur, algorithme, taille, temps_mesure, donees):
    """
    Sauvegarde les mesures dans la base de données.
    """
    date_heure = time.strftime('%Y-%m-%d %H:%M:%S',)
    connection = sqlite3.connect('donnees_mesurees.db')
    cursor = connection.cursor()

    # Sélectionnez la table appropriée en fonction de l'algorithme
    if algorithme == "Tri par Insertion":
        table_name = "mesures_insertion"
    elif algorithme == "Tri par Sélection":
        table_name = "mesures_selection"
    elif algorithme == "Tri avec Sorted":
        table_name = "mesures_sorted"
    elif algorithme == "Tri par bulles":
        table_name = "mesures_bulle"
    elif algorithme == "Tri par fusion":
        table_name = "mesures_fusion"

    # Vérifier si la table existe, sinon la créer et insérer les données
    cursor.execute(f"CREATE TABLE IF NOT EXISTS {table_name} (id INTEGER PRIMARY KEY, nom_utilisateur TEXT, algorithme TEXT, taille INTEGER, temps_mesure REAL, date_heure TEXT)")
    cursor.execute(f"INSERT INTO {table_name} (nom_utilisateur, algorithme, taille, temps_mesure, date_heure) VALUES (?, ?, ?, ?, ?)", (nom_utilisateur, algorithme, (taille + 1), temps_mesure, date_heure))
    connection.commit()
    cursor.close()
    connection.close()

def sauvegarde_elements(taille, elements):
    """
    Sauvegarde les éléments non triés dans la base de données.
    """
    connection = sqlite3.connect('donnees_mesurees.db')
    cursor = connection.cursor()

    # Convertir la liste en une chaîne JSON
    elements_json = json.dumps(elements)

    cursor.execute("CREATE TABLE IF NOT EXISTS elements_non_tries (id INTEGER PRIMARY KEY, taille INTEGER, elements TEXT)")
    cursor.execute("INSERT INTO elements_non_tries (taille, elements) VALUES (?, ?)", ((taille+1), elements_json))
    connection.commit()
    cursor.close()
    connection.close()

def liste_existe(taille):
    """
    Vérifie si une liste de la taille donnée existe dans la base de données.
    """
    connection = sqlite3.connect('donnees_mesurees.db')
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM elements_non_tries WHERE taille=?", (taille,))
    rangee = cursor.fetchall()
    cursor.close()
    connection.close()

    return bool(rangee)

def recuperer_elements(taille):
    """
    Récupère les éléments non triés de la base de données.
    """
    if liste_existe(taille):
        connection = sqlite3.connect('donnees_mesurees.db')
        cursor = connection.cursor()
        cursor.execute("SELECT elements FROM elements_non_tries WHERE taille=?", (taille,))
        elements_rangee = cursor.fetchone()
        cursor.close()
        connection.close()

        if elements_rangee:
            return json.loads(elements_rangee[0])
        else:
            return None
    else:
        return None
